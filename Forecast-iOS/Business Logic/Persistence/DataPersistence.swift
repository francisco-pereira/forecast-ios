//
//  DataPersistence.swift
//  Forecast-iOS
//
//  Created by Francisco Pereira on 24/07/2018.
//  Copyright © 2018 Francisco Pereira. All rights reserved.
//

import Foundation
import CoreData

class DataPersistence {
    
    var mainContext: NSManagedObjectContext?
    
    private var cities: [City]?
    
    init() {
        mainContext = persistentContainer.viewContext
    }
    
    func saveCities(cities: [City]) {
        self.cities = cities
        guard let context = mainContext else {
            fatalError("Context not initialised")
        }
        // Insert a new Entity
        var array: [NSManagedObject] = []
        for city in cities {
            let entity = NSEntityDescription.insertNewObject(forEntityName: "CityEntity", into: context)
            entity.setValue(city.id, forKey: "cityId")
            entity.setValue(city.name, forKey: "name")
            let coordinates = NSEntityDescription.insertNewObject(forEntityName: "CityCoordinatesEntity", into: context)
            coordinates.setValue(city.coord.lat, forKey: "latitude")
            coordinates.setValue(city.coord.lon, forKey: "longitude")
            
            entity.setValue(coordinates, forKey: "coordinate")
            
            array.append(entity)
        }
        try! context.save()
    }
    
    func loadCities() -> [City]? {
        guard let context = mainContext else {
            fatalError("Context not initialised")
        }
        
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "CityEntity")
        request.returnsObjectsAsFaults = false
        
        do {
            let result = try context.fetch(request) as? [NSManagedObject]
            let cities = result?.compactMap ({ (cityEntity) -> City in
                let coordinates = cityEntity.value(forKey: "coordinate") as! CityCoordinatesEntity
                return City(id: cityEntity.value(forKey: "cityId") as! Int,
                     name: cityEntity.value(forKey: "name") as! String,
                     country: nil, coord: Coordinates(lat: coordinates.latitude, lon: coordinates.longitude))
            })
            self.cities = cities
            return cities
        } catch {
            print("Failed")
        }
        return nil
    }
    
    func loadAllCities() -> [City]? {
        return self.cities
    }
    
    func loadCity(by name: String) -> [City]? {
        return cities?.filter({ (city) -> Bool in
            city.name.lowercased().contains(name.lowercased())
        })
    }
    
    lazy var persistentContainer: NSPersistentContainer = {
        let container = NSPersistentContainer(name: "ForecastModel")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    // MARK: - Core Data Saving support
    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    
}
