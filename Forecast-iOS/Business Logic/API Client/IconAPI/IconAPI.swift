//
//  IconAPI.swift
//  Forecast-iOS
//
//  Created by Francisco Pereira on 30/07/2018.
//  Copyright © 2018 Francisco Pereira. All rights reserved.
//

import Foundation
import UIKit

struct IconAPI {
    
    private let host = "https://png.icons8.com/"
    private let imageCache = NSCache<NSString, UIImage>()
    
    // need to use a tranformation because sometimes the api can't find an icon by name
    func IconTransform(name: String) ->  String {
        switch name {
        case "clouds":
            return "cloud"
        case "clear":
            return "sun"
        default:
            return name
        }
    }
    
    //  Using icons8.com to download the icons.
    func get(icon name: String, completion: @escaping(UIImage?, Error?, _ cached: Bool ) -> Void) {
        
        let name = IconTransform(name: name)
        
        let urlString = host + name
        
        if let cachedImage = imageCache.object(forKey: urlString as NSString) {
            completion(cachedImage, nil, true)
            return
        }
        
        guard let url = URL(string: urlString) else {
            let error = NSError(domain: "", code: 500, userInfo: [NSLocalizedDescriptionKey : "Failed to convert from url string to url \(urlString)"])
            completion(nil, error as Error, false)
            return
        }
        
        self.downloadImage(url: url, completion: { (image, error) in
            guard let image = image, error == nil else {
                completion(nil, error, false); return
            }
            
            // Cache image
            self.imageCache.setObject(image, forKey: urlString as NSString)
            DispatchQueue.main.async {
                completion(image, nil, false)
            }
        })
    }
    
    // Download image
    private func downloadImage(url: URL, completion: @escaping (_ image: UIImage?, _ error: Error? ) -> Void) {
        URLSession(configuration: .default).dataTask(with: url) { data, response, error in
            if let error = error {
                completion(nil, error)
                
            } else if let data = data, let image = UIImage(data: data) {
                completion(image, nil)
            } else {
                completion(nil, NSError(domain: "", code: 500, userInfo: [NSLocalizedDescriptionKey : "Failed to download an Image with url \(url.absoluteString)"]) as Error)
            }
            }.resume()
    }
}
