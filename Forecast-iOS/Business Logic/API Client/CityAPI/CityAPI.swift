//
//  CityAPI.swift
//  Forecast-iOS
//
//  Created by Francisco Pereira on 30/07/2018.
//  Copyright © 2018 Francisco Pereira. All rights reserved.
//

import Foundation

struct CityAPI {
    
    let dataPersistence: DataPersistence
    let config: ForecastAPIConfiguration
    
    func getCities(by country: String?, completionHandler: @escaping([City]?, ForecastAPIClientError?) -> Swift.Void) {
        if let cities = self.dataPersistence.loadCities(), cities.count > 0 {
            completionHandler(cities, nil)
            return
        }
        
        guard let url = Bundle.main.url(forResource: "city.list", withExtension: "json") else {
            completionHandler(nil, .invalidURL); return
        }
        
        let country = country ?? config.countryID
        
        DispatchQueue.global(qos: .background).async {
            do {
                let data = try Data.init(contentsOf: url, options: Data.ReadingOptions.uncached)
                let cities = try JSONDecoder().decode([City].self, from: data)
                
                let citiesByCountry = cities.filter ({ $0.country == country })
                
                self.dataPersistence.saveCities(cities: citiesByCountry)
                
                DispatchQueue.main.async {
                    completionHandler(citiesByCountry, nil)
                }
            }
            catch {
                print(error)
                completionHandler(nil, .parseError)
            }
        }
    }
}
