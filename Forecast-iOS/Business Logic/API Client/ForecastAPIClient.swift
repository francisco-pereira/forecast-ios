//
//  ForecastAPIClient.swift
//  Forecast-iOS
//
//  Created by Francisco Pereira on 24/07/2018.
//  Copyright © 2018 Francisco Pereira. All rights reserved.
//

import Foundation

enum Enviroment: String {
    case dev = "AppConfigDev"
    case prod = "TODO"
}

enum Result<Value> {
    case success(Value)
    case failure(Error)
}

enum ForecastAPIClientError: Error {
    case invalidURL
    case parseError
    case failed
}

class ForecastAPIClient {
    
    private let config: ForecastAPIConfiguration
    let dataPersistence = DataPersistence()
    
    let iconAPI: IconAPI
    let weatherAPI: WeatherAPI
    let cityAPI: CityAPI
    
    init(environment: Enviroment) {
        config = ForecastAPIConfiguration(environment: environment)
        weatherAPI = WeatherAPI(config: config)
        iconAPI = IconAPI()
        cityAPI = CityAPI(dataPersistence: dataPersistence, config: config)
    }
}

struct ForecastAPIConfiguration {
    
    private let fileExtension = "plist"
    private let HostKey = "HOST"
    private let SecretKey = "SECRET"
    private let CountryIDKey = "COUNTRY_ID"
    
    internal let host: String
    internal let secret: String
    internal let countryID: String
    
    init(environment: Enviroment) {
        let path = Bundle.main.path(forResource: environment.rawValue, ofType: fileExtension)
        let dic = NSDictionary(contentsOfFile: path!) as! [String: Any]
        host = dic[HostKey] as! String
        secret = dic[SecretKey] as! String
        countryID = dic[CountryIDKey] as! String
    }
    
}

