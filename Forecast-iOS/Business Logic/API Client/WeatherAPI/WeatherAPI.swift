//
//  WeatherAPI.swift
//  Forecast-iOS
//
//  Created by Francisco Pereira on 30/07/2018.
//  Copyright © 2018 Francisco Pereira. All rights reserved.
//

import Foundation

struct WeatherAPI {
    
    let config: ForecastAPIConfiguration
    
    func getWeather(cityID: String, completionHandler: @escaping(Forecast?, ForecastAPIClientError?) -> Swift.Void) {
        
        guard var urlComponent = URLComponents(string: config.host) else {
            fatalError("ForecastAPIClient: enable to get the host")
        }
        
        let cityIDURLQuery = URLQueryItem(name: "id", value: cityID)
        let appIDURLQuery = URLQueryItem(name: "appid", value: config.secret)
        let unitsURLQuery = URLQueryItem(name: "units", value: "metric")
        
        urlComponent.queryItems = [cityIDURLQuery, unitsURLQuery, appIDURLQuery]
        
        guard let url = urlComponent.url else {
            completionHandler(nil, .invalidURL)
            return
        }
        
        URLSession.shared.dataTask(with: url) { data,response,error in
            guard
                let jsonData = data,
                error == nil
                else {
                    print(error.debugDescription)
                    completionHandler(nil, .failed)
                    return
            }
            
            do {
                let decoder = JSONDecoder()
                let forecast = try decoder.decode(Forecast.self, from: jsonData)
                print(forecast)
                completionHandler(forecast, nil)
            } catch {
                completionHandler(nil, .parseError)
            }
            
            }.resume()
    }
    
}
