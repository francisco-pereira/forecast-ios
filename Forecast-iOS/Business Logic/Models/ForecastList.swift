//
//  ForecastList.swift
//  Forecast-iOS
//
//  Created by Francisco Pereira on 29/07/2018.
//  Copyright © 2018 Francisco Pereira. All rights reserved.
//

import Foundation

struct ForecastList: Decodable {
    var dt: Double
    var main: Temperature?
    var weather: [Weather]?
    var clouds: Clouds?
    var wind: Wind?
    var rain: Rain?
    var snow: Snow?
}
