//
//  Temperature.swift
//  Forecast-iOS
//
//  Created by Francisco Pereira on 29/07/2018.
//  Copyright © 2018 Francisco Pereira. All rights reserved.
//

import Foundation

struct Temperature: Decodable {
    let temp: Double
    let tempMin: Double
    let tempMax: Double
    let pressure: Double
    let seaLevel: Double
    let groundLevel: Double
    let humidity: Int
    
    enum CodingKeys: String, CodingKey {
        case temp
        case tempMin = "temp_min"
        case tempMax = "temp_max"
        case pressure
        case seaLevel = "sea_level"
        case groundLevel = "grnd_level"
        case humidity
    }
}
