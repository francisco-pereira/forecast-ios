//
//  Weather.swift
//  Forecast-iOS
//
//  Created by Francisco Pereira on 29/07/2018.
//  Copyright © 2018 Francisco Pereira. All rights reserved.
//

import Foundation

struct Weather: Decodable {
    let id: Int
    let main: String
    let description: String
    let icon: String
}
