//
//  Clouds.swift
//  Forecast-iOS
//
//  Created by Francisco Pereira on 29/07/2018.
//  Copyright © 2018 Francisco Pereira. All rights reserved.
//

import Foundation

struct Clouds: Decodable {
    let all: Int
}
