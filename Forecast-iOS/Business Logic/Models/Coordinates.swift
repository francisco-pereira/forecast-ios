//
//  Coordinates.swift
//  Forecast-iOS
//
//  Created by Francisco Pereira on 31/07/2018.
//  Copyright © 2018 Francisco Pereira. All rights reserved.
//

import Foundation

struct Coordinates: Decodable {
    let lat: Double
    let lon: Double
}
