//
//  City.swift
//  Forecast-iOS
//
//  Created by Francisco Pereira on 29/07/2018.
//  Copyright © 2018 Francisco Pereira. All rights reserved.
//

import Foundation

struct City: Decodable {
    let id: Int
    let name: String
    var country: String?
    let coord: Coordinates
}
