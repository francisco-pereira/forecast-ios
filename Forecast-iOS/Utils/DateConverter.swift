//
//  DateConverter.swift
//  Forecast-iOS
//
//  Created by Francisco Pereira on 31/07/2018.
//  Copyright © 2018 Francisco Pereira. All rights reserved.
//

import Foundation

class DateConverter {
    static let shared = DateConverter()
    
    let hourDateFormatter: DateFormatter = {
        let h = DateFormatter()
        h.timeStyle = .short
        h.dateStyle = .short
        return h
    }()
    
    func dateToHour(time: TimeInterval) -> String {
        let date = Date(timeIntervalSince1970: time)
        return hourDateFormatter.string(from: date)
    }
}
