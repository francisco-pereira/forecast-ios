//
//  TemperatureConverter.swift
//  Forecast-iOS
//
//  Created by Francisco Pereira on 31/07/2018.
//  Copyright © 2018 Francisco Pereira. All rights reserved.
//

import Foundation

struct TemperatureConverter {
    
    static func fahrenheitToCelsius(fahrenheit: Double) -> Double {
        return (fahrenheit - 32) / 1.8
    }
    
    static func fahrenheitToCelsius(fahrenheit: Double) -> String {
        let fahrenheit: Double = fahrenheitToCelsius(fahrenheit: fahrenheit)
        return String(fahrenheit) + "ºC"
    }
    
}
