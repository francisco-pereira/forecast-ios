//
//  AppRouter.swift
//  Forecast-iOS
//
//  Created by Francisco Pereira on 24/07/2018.
//  Copyright © 2018 Francisco Pereira. All rights reserved.
//

import UIKit

// TODO: Remove from AppRouter any responsability regards to add dependencies to any objects.
class AppRouter {
    static let shared = AppRouter()
    private let apiClient = ForecastAPIClient(environment: AppDelegate.environment)
    
    // Open Launch Screen using the AppDelegate window
    func openLaunchScreen(with window: UIWindow) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        guard let vc = storyboard.instantiateViewController(withIdentifier: "Launch") as? LaunchViewController
            else {
                fatalError("Couldn't instantiate MainViewController")
        }
        
        // Dependencies: 
        vc.interator = LaunchInteractor(apiClient: apiClient)
        vc.presenter = LaunchPresenter(viewController: vc)
        vc.window = window
        
        window.rootViewController = vc
        window.makeKeyAndVisible()
    }
    
    // Open Main Screen using the AppDelegate window
    func openMainScreen(with window: UIWindow) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        guard let vc = storyboard.instantiateViewController(withIdentifier: "Main") as? MainViewController
            else {
                fatalError("Couldn't instantiate MainViewController")
        }
        
        // Dependencies:
        vc.interator = MainInteractor(dataPersistence: self.apiClient.dataPersistence)
        vc.presenter = MainPresenter(viewController: vc)
        
        let navigationController = UINavigationController(rootViewController: vc)
        ThemeStyle.apply(navigationController: navigationController)
        
        // Show the Navigation Controller and the Main Screen
        window.rootViewController = navigationController
        window.makeKeyAndVisible()
    }
    
    // Open Details Screen using a navigation Controller
    func openDetailScreen(with navigationController: UINavigationController?, city: City) {
        guard let navigationController = navigationController else {
            fatalError("Couldn't find the navigation controller")
        }
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        guard let vc = storyboard.instantiateViewController(withIdentifier: "Detail") as? DetailViewController
            else {
                fatalError("Couldn't instantiate DetailViewController")
        }
        
        vc.city = city
        vc.interator = DetailInteractor(apiClient: apiClient)
        vc.presenter = DetailPresenter()
        
        navigationController.pushViewController(vc, animated: true)
    }
    
    func openMap(detailVC: DetailViewController, item: UIBarButtonItem, coordinate: Coordinates) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        guard let vc = storyboard.instantiateViewController(withIdentifier: "Map") as? MapViewController
        else {
            fatalError("Couldn't instantiate MapViewController")
        }
        
        vc.coordination = coordinate
        vc.preferredContentSize = CGSize(width: 300, height: 300)
        
        vc.modalPresentationStyle = .popover
        if let presentation = vc.popoverPresentationController {
            presentation.barButtonItem = item
            presentation.permittedArrowDirections = [.down, .up]
        }
        // TODO: show as a popover
//        detailVC.present(vc, animated: true, completion: nil)
        detailVC.navigationController?.pushViewController(vc, animated: true)
    }
}
