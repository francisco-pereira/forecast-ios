//
//  AppDelegate.swift
//  Forecast-iOS
//
//  Created by Francisco Pereira on 24/07/2018.
//  Copyright © 2018 Francisco Pereira. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    static let environment: Enviroment = .dev

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        let screenBounds = UIScreen.main.bounds
        window = UIWindow(frame: screenBounds)
        
        AppRouter.shared.openLaunchScreen(with: window!)
        
        return true
    }


}

