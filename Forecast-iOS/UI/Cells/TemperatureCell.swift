//
//  TemperatureCell.swift
//  Forecast-iOS
//
//  Created by Francisco Pereira on 30/07/2018.
//  Copyright © 2018 Francisco Pereira. All rights reserved.
//

import UIKit

class TemperatureCell: UICollectionViewCell {
    
    let iconAPI = IconAPI()
    
    @IBOutlet weak var hourLabel: UILabel!
    
    @IBOutlet weak var rainPercentageLabel: UILabel!
    @IBOutlet weak var temperatureLabel: UILabel!
    @IBOutlet weak var iconImageView: UIImageView!
    
    @IBOutlet weak var rainIconImageView: UIImageView!
    
    var forecast: ForecastList? {
        didSet {
            hourLabel.text = DateConverter.shared.dateToHour(time: forecast?.dt ?? 0)
            rainPercentageLabel.text = String(format: "%.2f", forecast?.rain?.lastThreeHours ?? 0) + "%"
            temperatureLabel.text = String(Int(forecast?.main?.temp ?? 0)) + "ºC"
            
            let iconName = forecast?.weather?.first?.main.lowercased() ?? "sun"
            iconAPI.get(icon: iconName) { (image, _, _) in
                DispatchQueue.main.async {
                    self.iconImageView.image = image
                }
            }
            
            iconAPI.get(icon: "rain") { (image, error, cached) in
                DispatchQueue.main.async {
                    self.rainIconImageView.image = image
                }
            }
        }
    }
    
}
