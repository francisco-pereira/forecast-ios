//
//  WeatherCell.swift
//  Forecast-iOS
//
//  Created by Francisco Pereira on 30/07/2018.
//  Copyright © 2018 Francisco Pereira. All rights reserved.
//

import UIKit

class WeatherCell: UICollectionViewCell {
    let iconAPI = IconAPI()
    
    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var temperatureLabel: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    
    private let CellIdentifier = "CellIdentifier"
    
    var forecast: Forecast? {
        didSet {
            let iconName = forecast?.list.first?.weather?.first?.main.lowercased() ?? "sun"
            self.iconAPI.get(icon: iconName) { (image, _, _) in
                DispatchQueue.main.async {
                    self.iconImageView.image = image
                }
            }
            cityLabel.text = "Now"
            temperatureLabel.text = String(Int(forecast?.list.first?.main?.temp ?? 0)) + "ºC"
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        collectionView.dataSource = self
        collectionView.register(UINib(nibName: "TemperatureCell", bundle: nil), forCellWithReuseIdentifier: CellIdentifier)
    }
}

extension WeatherCell: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CellIdentifier, for: indexPath) as? TemperatureCell
        
        cell?.forecast = forecast?.list[indexPath.row]
        
        return cell ?? UICollectionViewCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return forecast?.list.count ?? 0
    }
    
}
