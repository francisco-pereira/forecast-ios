//
//  ViewController.swift
//  Forecast-iOS
//
//  Created by Francisco Pereira on 24/07/2018.
//  Copyright © 2018 Francisco Pereira. All rights reserved.
//

import UIKit

// TODO: there's some UX improvements need with the Search bar
class MainViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    
    private let CellIdentifier = "CellIdentier"
    
    let searchController = UISearchController(searchResultsController: nil)
    
    var presenter: MainPresenter!
    var interator: MainInteractor!
    
    var dataSource: [City] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.interator.locationDelegate = self
        
        // Navigation bar
        let locationItem = UIBarButtonItem(image: #imageLiteral(resourceName: "baseline_location_on_black_36pt"), style: .plain, target: self, action: #selector(clickedLocationButton(sender:)))
        navigationItem.rightBarButtonItem = locationItem
        
        // TableView:
        // register CityCell to be reused
        tableView.register(UINib(nibName: "CityCell", bundle: nil), forCellReuseIdentifier: CellIdentifier)
        
        // Setup the Search Controller
        searchController.searchBar.delegate = self
        searchController.searchBar.placeholder = "Search Location"
        navigationItem.searchController = searchController
        
        loadEmptyTable()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        searchController.searchBar.becomeFirstResponder()
    }
    
    @objc func clickedLocationButton(sender: AnyObject) {
        self.interator.getLocation()
    }
    
    func loadEmptyTable() {
        interator.searchCities(scope: "", completionHandler: { (cities) in
            self.dataSource = cities ?? []
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        })
    }
    
}

extension MainViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let city = dataSource[indexPath.row]
        presenter.openDetailScreen(city: city)
    }
}

extension MainViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let city = dataSource[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifier, for: indexPath) as? CityCell
        cell?.titleLabel.text = city.name
        
        return cell ?? UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource.count
    }
}

extension MainViewController: MainInteractorLocationDelegate {
    
    func location(found: City) {
        presenter.openDetailScreen(city: found)
    }
    
    func locationDenied() {
        presenter.locationDeniedError()
    }
}

extension MainViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText.count == 0 {
            return
        }
        interator.searchCities(scope: searchText) { cities in
            self.dataSource = cities ?? []
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }
    }
}

