//
//  MainInteractor.swift
//  Forecast-iOS
//
//  Created by Francisco Pereira on 24/07/2018.
//  Copyright © 2018 Francisco Pereira. All rights reserved.
//

import Foundation
import CoreLocation

protocol MainInteractorLocationDelegate: class {
    func location(found: City)
    func locationDenied()
}


class MainInteractor: NSObject {
    
    weak var locationDelegate: MainInteractorLocationDelegate?
    weak var dataPersistence: DataPersistence?
    
    convenience init(dataPersistence: DataPersistence) {
        self.init()
        self.dataPersistence = dataPersistence
    }
    
    lazy var locationManager: CLLocationManager = {
        let l = CLLocationManager()
        l.delegate = self
        return l
    }()
    
    func getLocation() {
        let status  = CLLocationManager.authorizationStatus()
        switch status {
            case .notDetermined:
                locationManager.requestWhenInUseAuthorization()
            case .denied, .restricted:
                locationDelegate?.locationDenied()
            default:
                locationManager.startUpdatingLocation()
        }
    }
    
    func searchCities(scope: String, completionHandler: @escaping([City]?) -> Swift.Void) {
        var cities: [City]?
        if scope.isEmpty == true {
            cities = dataPersistence?.loadAllCities()
        } else {
            cities = dataPersistence?.loadCity(by: scope)
        }
        
        completionHandler(cities)
    }
}

extension MainInteractor: CLLocationManagerDelegate {

    private func locationManager(manager: CLLocationManager!, didChangeAuthorizationStatus status: CLAuthorizationStatus) {
        switch status {
        case .notDetermined:
            manager.requestWhenInUseAuthorization()
        case .authorizedWhenInUse, .authorizedAlways:
            manager.startUpdatingLocation()
            break
        case .restricted, .denied:
            locationDelegate?.locationDenied()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let currentLocation = locations.last!
        print("Current location: \(currentLocation)")
        CLGeocoder().reverseGeocodeLocation(currentLocation, completionHandler: { (placemarks, _) -> Void in
            placemarks?.forEach { (placemark) in
                if let locality = placemark.locality {
                    if let city = self.dataPersistence?.loadCity(by: locality)?.first {
                        self.locationDelegate?.location(found: city)
                        return
                    }
                }
            }
        })
        
        
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Error \(error)")
    }
}
