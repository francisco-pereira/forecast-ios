//
//  MainPresenter.swift
//  Forecast-iOS
//
//  Created by Francisco Pereira on 24/07/2018.
//  Copyright © 2018 Francisco Pereira. All rights reserved.
//

import UIKit

class MainPresenter {
    
    weak var viewController: MainViewController?
    
    init(viewController: MainViewController) {
        self.viewController = viewController
    }
    
    func openDetailScreen(city: City) {
        AppRouter.shared.openDetailScreen(with: viewController?.navigationController, city: city)
    }
    
    func locationDeniedError() {
        let alert = UIAlertController(title: "Location Services Disabled", message: "Please enable Location Services in Settings", preferredStyle: .alert)
        
        let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alert.addAction(okAction)
        
        viewController?.present(alert, animated: true, completion: nil)
    }
}
