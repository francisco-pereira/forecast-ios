//
//  MapViewController.swift
//  Forecast-iOS
//
//  Created by Francisco Pereira on 31/07/2018.
//  Copyright © 2018 Francisco Pereira. All rights reserved.
//

import UIKit
import MapKit

class MapViewController: UIViewController {
    @IBOutlet weak var mapView: MKMapView!
    
    private let regionRadius: CLLocationDistance = 1000
    
    var coordination: Coordinates!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let initialLocation = CLLocation(latitude: coordination!.lat, longitude: coordination!.lon)
        let coordinateRegion = MKCoordinateRegionMakeWithDistance(initialLocation.coordinate, self.regionRadius, self.regionRadius)
        self.mapView.setRegion(coordinateRegion, animated: true)
    }
    
    
    func adaptivePresentationStyle(for controller: UIPresentationController, traitCollection: UITraitCollection) -> UIModalPresentationStyle {
        // return UIModalPresentationStyle.FullScreen
        return UIModalPresentationStyle.none
    }
}
