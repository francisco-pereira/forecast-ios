//
//  LaunchViewController.swift
//  Forecast-iOS
//
//  Created by Francisco Pereira on 24/07/2018.
//  Copyright © 2018 Francisco Pereira. All rights reserved.
//

import UIKit

class LaunchViewController: UIViewController {
    
    weak var window: UIWindow!
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    var presenter: LaunchPresenter!
    var interator: LaunchInteractor!
    
    @IBOutlet weak var logoImageView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.startActivityIndicator()
        loadCities()
    }
    
    func loadCities() {
        interator.loadCities { (error) in
            if error != nil {
                self.presenter.errorMessage(viewController: self)
                return
            }
            self.presenter.stopActivityIndicator()
            self.presenter.openMainScreen(window: self.window)
        }
    }
    
}
