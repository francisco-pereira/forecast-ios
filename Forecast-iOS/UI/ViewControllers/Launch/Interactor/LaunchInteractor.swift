//
//  LaunchInteractor.swift
//  Forecast-iOS
//
//  Created by Francisco Pereira on 30/07/2018.
//  Copyright © 2018 Francisco Pereira. All rights reserved.
//

import Foundation

class LaunchInteractor {
    
    let apiClient: ForecastAPIClient
    
    init(apiClient: ForecastAPIClient) {
        self.apiClient = apiClient
    }
    
    func loadCities(completionHandler: @escaping(ForecastAPIClientError?) -> Swift.Void) {
        let country: String? = nil // load default country
        apiClient.cityAPI.getCities(by: country) { (_, error) in
            completionHandler(error)
        }
    }
    
}
