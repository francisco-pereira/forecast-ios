//
//  LaunchPresenter.swift
//  Forecast-iOS
//
//  Created by Francisco Pereira on 24/07/2018.
//  Copyright © 2018 Francisco Pereira. All rights reserved.
//

import UIKit

class LaunchPresenter {
    
    weak var viewController: LaunchViewController?
    
    init(viewController: LaunchViewController) {
        self.viewController = viewController
    }
    
    func openMainScreen(window: UIWindow) {
        AppRouter.shared.openMainScreen(with: window)
    }
    
    func startActivityIndicator() {
        DispatchQueue.main.async {
            self.viewController?.activityIndicator.startAnimating()
        }
    }
    
    func stopActivityIndicator() {
        DispatchQueue.main.async {
            self.viewController?.activityIndicator.stopAnimating()
        }
    }
    
    func errorMessage(viewController: LaunchViewController) {
        let alert = UIAlertController(title: "Error", message: "Please check your network and try again", preferredStyle: .alert)
        alert.addAction(UIAlertAction.init(title: "Retry", style: .default, handler: { (action) in
            viewController.loadCities()
        }))
        DispatchQueue.main.async {
            viewController.present(alert, animated: true, completion: nil)
        }
    }

}
