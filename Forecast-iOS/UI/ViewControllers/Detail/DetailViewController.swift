//
//  DetailViewController.swift
//  Forecast-iOS
//
//  Created by Francisco Pereira on 24/07/2018.
//  Copyright © 2018 Francisco Pereira. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {
    private let CellIdentifier = "CellIdentifier"
    @IBOutlet weak var collectionView: UICollectionView!
    
    var presenter: DetailPresenter!
    var interator: DetailInteractor!
    var city: City!
    
    internal var forecast: Forecast?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = city.name
        
        // Navigation bar
        let locationItem = UIBarButtonItem(image:#imageLiteral(resourceName: "baseline_map_black_36pt"), style: .plain, target: self, action: #selector(clickedMapButton(sender:)))
        navigationItem.rightBarButtonItem = locationItem
        
        // Set Collection View
        collectionView.dataSource = self
        collectionView.register(UINib(nibName: "WeatherCell", bundle: nil), forCellWithReuseIdentifier: CellIdentifier)
        
        // Load Data
        loadForecast()
    }
    
    @objc func clickedMapButton(sender: UIBarButtonItem) {
        self.presenter.openMap(vc: self, item: sender, coordination: city.coord)
    }
    
    func loadForecast() {
        interator.getForecast(city: city) { (forecast) in
            self.forecast = forecast
            DispatchQueue.main.async {
                self.collectionView.reloadData()
            }
        }
    }
}

extension DetailViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let collectionViewSize = collectionView.frame.size
        return CGSize(width: collectionViewSize.width, height: collectionViewSize.height)
    }
    
}

extension DetailViewController: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CellIdentifier, for: indexPath) as? WeatherCell
        cell?.forecast = self.forecast
        
        return cell ?? UICollectionViewCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return forecast?.list.count ?? 0
    }
    
}
