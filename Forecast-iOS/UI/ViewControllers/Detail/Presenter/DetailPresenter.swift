//
//  DetailPresenter.swift
//  Forecast-iOS
//
//  Created by Francisco Pereira on 24/07/2018.
//  Copyright © 2018 Francisco Pereira. All rights reserved.
//

import UIKit

struct DetailPresenter {
    
    func openMap(vc: DetailViewController, item: UIBarButtonItem, coordination: Coordinates) {
        AppRouter.shared.openMap(detailVC: vc, item: item, coordinate: coordination)
    }
}
