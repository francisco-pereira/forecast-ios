//
//  DetailInteractor.swift
//  Forecast-iOS
//
//  Created by Francisco Pereira on 24/07/2018.
//  Copyright © 2018 Francisco Pereira. All rights reserved.
//

import Foundation

struct DetailInteractor {
    
    let apiClient: ForecastAPIClient
    
    func getForecast(city: City, completionHandler: @escaping(Forecast?) -> Swift.Void) {
        apiClient.weatherAPI.getWeather(cityID: String(city.id)) { (forecast, error) in
            completionHandler(forecast)
        }
    }
}
