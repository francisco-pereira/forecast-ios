//
//  Forecast_iOSTests.swift
//  Forecast-iOSTests
//
//  Created by Francisco Pereira on 24/07/2018.
//  Copyright © 2018 Francisco Pereira. All rights reserved.
//

import XCTest
@testable import Forecast_iOS

class ForecastAPIClientTest: XCTestCase {
    
    let forecastAPIClient = ForecastAPIClient(environment: .dev)
    
    override func setUp() {
        super.setUp()
        print("ForecastAPIClientTest started")
    }
    
    override func tearDown() {
        super.tearDown()
        print("teardown")
    }
    
    // test get weather for Moscow
    func testGetWeather() {
        let expectation = XCTestExpectation(description: "Download forecast")
        
        forecastAPIClient.weatherAPI.getWeather(cityID: "524901") { (forecast, error) in
            guard let _ = forecast, error == nil else {
                XCTFail("Failed to return the weather"); return
            }
            expectation.fulfill()
        }
        
        // Wait until the expectation is fulfilled
        wait(for: [expectation], timeout: 20.0)
    }
    
    func testGetCities() {
        let expectation = XCTestExpectation(description: "Get cities")
        
        forecastAPIClient.cityAPI.getCities(by: "GB", completionHandler: { (cities, error) in
            XCTAssert(error == nil, "Failed to return array of cities: \(error!.localizedDescription)")
            XCTAssert(cities != nil, "Empty return")
            
            expectation.fulfill()
        })
        
        // Wait until the expectation is fulfilled
        wait(for: [expectation], timeout: 40.0)
    }
    
    func testGetSunIcon() {
        let expectation = XCTestExpectation(description: "Get Sun Icon")
        
        IconAPI().get(icon: "sun") { (image, error, cached) in
            XCTAssert(error == nil, "Error: \(error.debugDescription)")
            XCTAssert(image != nil)
            expectation.fulfill()
        }
        
        wait(for: [expectation], timeout: 5.0)
    }
    
}
