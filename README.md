# TUI Mobility Hub - Test Project Challenge
# OpenWeather

## Description:
The App Forecast was architect using VIPER which helped to separate responsibilities and to make the code very clean.
To develop the app I used the TDD principle with help of 'MyPlayground' to create my Unit Tests and make sure the app is solid.
There are some 'todos' that I left behind and also design improvements.
As suggested I implemented all the Bonus: download & caching the icons, search bar to filter the locations and showing location on maps.

## Exercise

### Use the OpenWeather API call http://openweathermap.org/forecast5 to retrieve data for displaying weather forecast by every 3 hours for upcoming 5 days for specific city in UK.

### Present data in visual layout, where every day represents a row and every 3 hour weather forecast represents a cell in a row. User has to be able to scroll the cells within the row.

### Each row should clearly show date and each cell in the row should clearly show temperature and time of the day. Present city/town/postcode as well, whatever applicable.
* Bonus 1: Show weather icon in each cell (download and cache it, don’t bundle it in the App).
* Bonus 2: Show weather for specific location within UK.
* Bonus 3: Show map for location.

### Instructions:
- Don’t pay too much attention to design, use standard UI elements.
- Consider running it on different devices and orientations.
- Code in Swift 4.
- Use iOS SDK version of your choice.
- Don’t use any 3rd party tools/frameworks.
- Return Xcode 9+ project, zipped and few words about your code.
- Do Test-driven Development.


## Architecture
Using VIPER

## TDD
Please check the MyPlayground located at the root of the project and the unit-tests.
