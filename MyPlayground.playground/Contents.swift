//: Playground - noun: a place where people can play

import XCTest
import Foundation
import CoreData
import AppKit

print("Playground started executing")

struct Forecast: Decodable {
    let city: City
    let list: [ForecastList]
    
}

struct City: Decodable {
    let id: Int
    let name: String
    var country: String?
}

struct ForecastList: Decodable {
    var main: Temperature?
    var weather: [Weather]?
    var clouds: Clouds?
    var wind: Wind?
    var rain: Rain?
    var snow: Snow?
}

struct Temperature: Decodable {
    let temp: Double
    let tempMin: Double
    let tempMax: Double
    let pressure: Double
    let seaLevel: Double
    let groundLevel: Double
    let humidity: Int
    
    enum CodingKeys: String, CodingKey {
        case temp
        case tempMin = "temp_min"
        case tempMax = "temp_max"
        case pressure
        case seaLevel = "sea_level"
        case groundLevel = "grnd_level"
        case humidity
    }
}

struct Weather: Decodable {
    let id: Int
    let main: String
    let description: String
    let icon: String
}

struct Clouds: Decodable {
    let all: Int
}

struct Wind: Decodable {
    let speed: Double
    let deg: Double
}

struct Rain: Decodable {
    var lastThreeHours: Double?
    
    enum CodingKeys: String, CodingKey {
        case lastThreeHours = "3h"
    }
}

struct Snow: Decodable {
    var lastThreeHours: Double?
    
    enum CodingKeys: String, CodingKey {
        case lastThreeHours = "3h"
    }
}

enum ForecastAPIClientError: Error {
    case invalidURL
    case parseError
    case failed
}

class ForecastAPIClient {
    
    var urlString: String = "http://samples.openweathermap.org/data/2.5/forecast?id=524901&appid=b6907d289e10d714a6e88b30761fae22"
    
    func getWeather(completionHandler: @escaping(Forecast?, ForecastAPIClientError?) -> Swift.Void) {
        guard let url = URL.init(string: urlString) else {
            completionHandler(nil, .invalidURL)
            return
        }
        
        URLSession.shared.dataTask(with: url) { data,response,error in
            guard
                let jsonData = data,
                error == nil
            else {
                print(error.debugDescription)
                completionHandler(nil, .failed)
                return
            }

            do {
                let decoder = JSONDecoder()
                let forecast = try decoder.decode(Forecast.self, from: jsonData)
                completionHandler(forecast, nil)
            } catch {
                completionHandler(nil, .parseError)
            }

        }.resume()
    }
    
    func getCities(by country: String, completionHandler: @escaping([City]?, ForecastAPIClientError?) -> Swift.Void) {
        guard let url = Bundle.main.url(forResource: "city.list", withExtension: "json") else {
            completionHandler(nil, .invalidURL); return
        }
        
        DispatchQueue.global(qos: .background).async {
            do {
                let data = try Data.init(contentsOf: url, options: Data.ReadingOptions.uncached)
                let cities = try JSONDecoder().decode([City].self, from: data)
                
                let citiesByCountry = cities.filter ({ $0.country == country })
                
                DispatchQueue.main.async {
                    completionHandler(citiesByCountry, nil)
                }
            }
            catch {
                print(error)
                completionHandler(nil, .parseError)
            }
        }
    }
}

class ForecastAPIClientTest: XCTestCase {
    
    let forecastAPIClient = ForecastAPIClient()
    
    override func setUp() {
        super.setUp()
        print("ForecastAPIClientTest started")
    }
    
    override func tearDown() {
        super.tearDown()
        print("teardown")
    }
    
    func testGetWeather() {
        let expectation = XCTestExpectation(description: "Download forecast")

        forecastAPIClient.getWeather { (forecast, error) in
            guard let _ = forecast, error == nil else {
                XCTFail("Failed to return the weather"); return
            }
            XCTAssert(forecast != nil, "failed getting the weather")
            expectation.fulfill()
        }
        
        // Wait until the expectation is fulfilled
        wait(for: [expectation], timeout: 10.0)
    }
    
    func testGetCities() {
        let expectation = XCTestExpectation(description: "Get cities")
        
        forecastAPIClient.getCities(by: "GB", completionHandler: { (cities, error) in
            XCTAssert(error == nil, "Failed to return array of cities: \(error!.localizedDescription)")
            XCTAssert(cities != nil, "Empty return")
            
            expectation.fulfill()
        })
        
        // Wait until the expectation is fulfilled
        wait(for: [expectation], timeout: 40.0)
    }
    
}

ForecastAPIClientTest.defaultTestSuite.run()


class DataPersistence {
    
    var mainContext: NSManagedObjectContext?
    
    init() {
        mainContext = persistentContainer.viewContext
    }
    
    func saveCities(cities: [City]) {
        guard let context = mainContext else {
            fatalError("Context not initialised")
        }
        // Insert a new Entity
        var array: [NSManagedObject] = []
        for city in cities {
            let entity = NSEntityDescription.insertNewObject(forEntityName: "CityEntity", into: context)
            entity.setValue(city.id, forKey: "cityId")
            entity.setValue(city.name, forKey: "name")
            
            array.append(entity)
        }
        try! context.save()
    }
    
    func loadCities() -> [City]? {
        guard let context = mainContext else {
            fatalError("Context not initialised")
        }
        
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "CityEntity")
        request.returnsObjectsAsFaults = false
        
        do {
            let result = try context.fetch(request) as? [NSManagedObject]
            return result?.compactMap ({ (cityEntity) -> City in
                    City(id: cityEntity.value(forKey: "cityId") as! Int,
                     name: cityEntity.value(forKey: "name") as! String,
                     country: nil)
                })
        } catch {
            print("Failed")
        }
        return nil
    }
    
    func loadCity(by name: String) -> [City]? {
        
        return nil
    }
    
    lazy var persistentContainer: NSPersistentContainer = {
        let container = NSPersistentContainer(name: "ForecastModel")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    // MARK: - Core Data Saving support
    func saveContext () {
        if mainContext?.hasChanges == true {
            do {
                try mainContext?.save()
            } catch {
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    
}

class DataPersistenceTest: XCTestCase {
    
    let dataPersistence = DataPersistence()
    let container = NSPersistentContainer(name: "ForecastModel")
    
    override func setUp() {
        super.setUp()
        container.loadPersistentStores { (description, error) in
            if let loadError = error {
                print("Error occured! \(loadError)")
                return
            }
        }
        dataPersistence.mainContext = container.viewContext
    }
    
    /* Todo:
     Unfortunately I was unable to access to Core Data Entity using Playground after following this website
     https://medium.com/@alekkania/using-coredata-in-xcode-playground-f9d508f11870
    func testSaveCities() {
        let expectation = XCTestExpectation(description: "Get and save cities")
        
        let apiClient = ForecastAPIClient()
        apiClient.getCities(by: "GB") { (cities, error) in
            guard let cities = cities, error == nil else {
                XCTAssert(error == nil, "Failed to return array of cities: \(error!.localizedDescription)"); return
            }
            
            self.dataPersistence.saveCities(cities: cities)
            expectation.fulfill()
        }
        
        // Wait until the expectation is fulfilled
        wait(for: [expectation], timeout: 40.0)
    }
    */
    
}

DataPersistenceTest.defaultTestSuite.run()

typealias UIImage = NSImage

struct IconAPI {
    
    private let host = "https://png.icons8.com/"
    private let imageCache = NSCache<NSString, UIImage>()
    
    //  Using icons8.com to download the icons.
    func get(icon name: String, completion: @escaping(UIImage?, Error?, _ cached: Bool ) -> Void) {
        
        let urlString = host + name
        
        if let cachedImage = imageCache.object(forKey: urlString as NSString) {
            completion(cachedImage, nil, true)
            return
        }
        
        guard let url = URL(string: urlString) else {
            let error = NSError(domain: "", code: 500, userInfo: [NSLocalizedDescriptionKey : "Failed to convert from url string to url \(urlString)"])
            completion(nil, error as Error, false)
            return
        }
        let request = URLRequest(url: url)
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
                    
        self.downloadImage(url: url, completion: { (image, error) in
            guard let image = image, error == nil else {
                completion(nil, error, false); return
            }
            
            // Cache image
            self.imageCache.setObject(image, forKey: urlString as NSString)
            DispatchQueue.main.async {
                completion(image, nil, false)
            }
        })
    }
    
    // Download image
    private func downloadImage(url: URL, completion: @escaping (_ image: UIImage?, _ error: Error? ) -> Void) {
        URLSession(configuration: .default).dataTask(with: url) { data, response, error in
            if let error = error {
                completion(nil, error)
                
            } else if let data = data, let image = UIImage(data: data) {
                completion(image, nil)
            } else {
                completion(nil, NSError(domain: "", code: 500, userInfo: [NSLocalizedDescriptionKey : "Failed to download an Image with url \(url.absoluteString)"]) as Error)
            }
        }.resume()
    }
}

class IconAPITest: XCTestCase {
    
    let iconAPI = IconAPI()
    
    func testGetSun() {
        let expectation = XCTestExpectation(description: "Get Sun Icon")
        
        iconAPI.get(icon: "sun") { (image, error, cached) in
            XCTAssert(error == nil, "Error: \(error.debugDescription)")
            XCTAssert(image != nil)
            expectation.fulfill()
        }
        
        wait(for: [expectation], timeout: 5.0)
    }
}

IconAPITest.defaultTestSuite.run()

